<?php

/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 **/

// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.
if (empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW'])) {
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="For lab6"');
    print('<h1>401 Требуется авторизация</h1>');
    exit();
}
$user = 'u21391';
$pass = '5188469';
$db = new PDO('mysql:host=localhost;dbname=u21391', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
$login=$_SERVER['PHP_AUTH_USER'];
$request = "SELECT * from admins where login ='$login'";
$result = $db->prepare($request);
$result->execute();
$flag = 0;
while ($data = $result->fetch()) {
    if ($data['login'] == $_SERVER['PHP_AUTH_USER'] && $data['password']==md5($_SERVER['PHP_AUTH_PW'])) {
        $flag = 1;
    }
}
if ($flag == 0) {
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="For laba6"');
    print('<h1>401 Требуется авторизация</h1>');
    exit();
}?>

<!DOCTYPE html>

<html lang="ru">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="admin_style.css">
        <title>Админка</title>
    </head>

    <body>
<?php print('Вы успешно авторизовались и видите защищенные паролем данные.');
setcookie('admin', '1');

// *********
// Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
// Реализовать просмотр и удаление всех данных.
// *********

$link = mysqli_connect("localhost", "u21391", "5188469", "u21391");
if (mysqli_connect_errno()) {
    printf("Не удалось подключиться: %s\n", mysqli_connect_error());
    exit();
}
$query = mysqli_query($link, "SELECT id, name, email, year, gender, limbs, biography FROM users");
//$row = mysqli_fetch_array($query, MYSQLI_ASSOC);
$table = "<table class='table'>" . "<caption>Пользователи</caption>";

$table .= "<thead>" . "<tr>";
$table .= "<th >id</th>";
$table .= "<th >Имя</th>";
$table .= "<th >E-mail</th>";
$table .= "<th >Дата рождения</th>";
$table .= "<th >Гендер</th>";
$table .= "<th >Кол-во конечностей</th>";
$table .= "<th >Биография</th>";
$table .= "</tr>" . "</thead>";
$table .= "<tbody>";

while($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
    $table .= "<tr>";
    $table .= "<td >".$row['id']."</td>";
    $table .= "<td >".$row['name']."</td>";
    $table .= "<td >".$row['email']."</td>";
    $table .= "<td >".$row['year']."</td>";
    $table .= "<td >".$row['gender']."</td>";
    $table .= "<td >".$row['limbs']."</td>";
    $table .= "<td >".$row['biography']."</td>";
    $table .= "</tr>";
}
$table .= "</tbody>" . "</table>";
print $table;

$query = mysqli_query($link, "SELECT id, id_user FROM abilities_of_users");
$table = "<table class='table'>" . "<caption>Связь пользователей с их способностями</caption>";

$table .= "<thead>" . "<tr>";
$table .= "<th >id способности</th>";
$table .= "<th >id пользователя</th>";
$table .= "</tr>" . "</thead>";
$table .= "<tbody>";

while($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
    $table .= "<tr>";
    $table .= "<td >".$row['id']."</td>";
    $table .= "<td >".$row['id_user']."</td>";
    $table .= "</tr>";
}
$table .= "</tbody>" . "</table>";
print $table;

$query = mysqli_query($link, "SELECT id_ability, ability_immortality, 
       ability_passing_through_walls, ability_levitation FROM abilities");
$table = "<table class='table'>" . "<caption>Способности пользователей</caption>";

$table .= "<thead>" . "<tr>";
$table .= "<th >id способности</th>";
$table .= "<th >Бессмертие</th>";
$table .= "<th >Прохождение сквозь стены</th>";
$table .= "<th >Левитация</th>";
$table .= "</tr>" . "</thead>";
$table .= "<tbody>";

while($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
    $table .= "<tr>";
    $table .= "<td >".$row['id_ability']."</td>";
    $ans = ($row['ability_immortality']==1) ? "&#10003;" : "&#10007;";
    $table .= "<td >".$ans."</td>";
    $ans = ($row['ability_passing_through_walls']==1) ? "&#10003;" : "&#10007;";
    $table .= "<td >".$ans."</td>";
    $ans = ($row['ability_levitation']==1) ? "&#10003;" : "&#10007;";
    $table .= "<td >".$ans."</td>";
    $table .= "</tr>";
}
$table .= "</tbody>" . "</table>";
print $table;

$query = mysqli_query($link, "SELECT ability_immortality, 
       ability_passing_through_walls, ability_levitation FROM abilities");
$table = "<table class='table'>" . "<caption>Статистика по сверхспособностям</caption>";

$table .= "<thead>" . "<tr>";
$table .= "<th >Бессмертие</th>";
$table .= "<th >Прохождение сквозь стены</th>";
$table .= "<th >Левитация</th>";
$table .= "</tr>" . "</thead>";
$table .= "<tbody>";

    $table .= "<tr>";
    $ans = "SELECT COUNT(ability_immortality) FROM abilities WHERE ability_immortality=1 GROUP BY ability_immortality";
    $result = $db->prepare($ans);
    $result->execute();
    $table .= "<td >".$result->fetch()[0]."</td>";
    $ans = "SELECT COUNT(ability_passing_through_walls) FROM abilities WHERE ability_passing_through_walls=1 GROUP BY ability_passing_through_walls";
    $result = $db->prepare($ans);
    $result->execute();
    $table .= "<td >".$result->fetch()[0]."</td>";
    $ans = "SELECT COUNT(ability_levitation) FROM abilities WHERE ability_levitation=1 GROUP BY ability_levitation";
    $result = $db->prepare($ans);
    $result->execute();
    $table .= "<td >".$result->fetch()[0]."</td>";
    $table .= "</tr>";

$table .= "</tbody>" . "</table>";
print $table;

$query = mysqli_query($link, "SELECT id, id_user, login, password FROM registered");
$table = "<table class='table'>" . "<caption>Зарегистрированные пользователи</caption>";

$table .= "<thead>" . "<tr>";
$table .= "<th >id</th>";
$table .= "<th >id пользователя</th>";
$table .= "<th >Логин</th>";
$table .= "<th >Хэш пароля</th>";
$table .= "</tr>" . "</thead>";
$table .= "<tbody>";

while($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
    $table .= "<tr>";
    $table .= "<td >".$row['id']."</td>";
    $table .= "<td >".$row['id_user']."</td>";
    $table .= "<td >".$row['login']."</td>";
    $table .= "<td >".$row['password']."</td>";
    $table .= "</tr>";
}
$table .= "</tbody>" . "</table>";
print $table;
?>

<form action="delete-script.php" method="post" accept-charset="UTF-8">
    <div class="set">
        <div class="delete">
            <label>
                <input class="inp" type="number" name="delete">
            </label>
        </div>

        <input type="submit" id="send" class="buttonform" value="Удалить запись по ID">
    </div>
</form>

<form action="change-script.php" method="post" accept-charset="UTF-8">
    <div class="set">
        <div class="change">
            <label>
                <input class="inp" type="number" name="change">
            </label>
        </div>
        <input type="submit" id="send" class="buttonform" value="Изменить запись по ID">
    </div>
</form>

<?php
mysqli_close($link);
?>
        <a href="login.php" class="button">Выйти</a>
    </body>
</html>
